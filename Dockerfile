FROM node
WORKDIR /src
COPY . .
EXPOSE 7000
CMD node server.js